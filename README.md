# KAJ_Hangman

## Cíle

Cílem projektu bylo vytvořit jednoduchou jednostránkovou aplikaci pro hraní hry oběšenec do předmětu KAJ.

## Postup

Nejprve jsem vytvořil funkční hru. Potom jsem přidal stránky na zobrazování výsledků. Když tahle smyčka fungovala, přidal jsem styly. Tabulku skóre jsem vytvářel až jako poslední.
Pro implementaci kostry aplikace jsem se inspiroval příklady z 11.cvičení.

## Popis aplikace
Aplikace se skládá ze tří stránek.
Stránka hry obsahuje abecedu v podobě tlačítek, zakrytá písmena hádaného slova a postupně se odhalujícího oběšence. Když hra skončí, přepne se stránka na zobrazení výsledků. Když hráč vyhraje (uhodne slovo), je zobrazeno jeho skóre v podobě počtu neodhalených částí oběšence. S tímto skóre se může zapsat do historie pod přezdívkou z malých a velkých písmen plus číslic. Pokud tak učiní, zobrazí se mu tabulka skóre. Ze stránky s výsledkem i ze stránky s tabulkou je možné okamžitě přejít zpět na stránku s hrou a hádat znovu jiné náhodné slovo.

### Zdrojový kód
Veškerý kód je v jednom HTML souboru. Skript je rozdělen do pěti tříd:
- [ ] Page - Pro zobrazování daného obsahu
- [ ] Game - Pro zobrazení a ovládání hry
- [ ] Results - Pro zobrazení výsledků a zpracování vstupu přezdívky hráče
- [ ] HighScoreTable - Pro zobrazení a ukládání tabulky skóre
- [ ] App - Pro řízení celké aplikace a přepínání mezi stránkama

## Implementovaná funkcionalita

### HTML
- [ ] Validita: HTML všech tří stránek byly otestovány w3validátorem
- [ ] Funguje na Chrome, Mozilla, Opera a Edge. Na ostatních prohlížečích nebyla testována
- [ ] Semantické značky: Doufám, že jsem žádnou značku nepoužil nesprávně. V HTML jsem zatím nikdy nedělal.
- [ ] SVG/Canvas: Oběšenec se vykresluje jako SVG
- [ ] Audio/Video: Nejspíš to je až do JS sekce, ale když uživatel hádá špatné písmeno, ozve se otravný bzučák. Když uhádne slovo ozve se druhý zvuk. Oba jsem si vypůjčil z přednášky.
- [ ] Formulářové prvky: Zadávání jména hráče po uhádnutí slova. Jen jedno políčko :(
- [ ] Offline: Není

### CSS
- [ ] Pseudotřídy: Pseudotřídy :nth-child - tabulka skóre, :hover - tlačítka, :valid - přezdívka hráče, :invalid - přezdívka hráče.
- [ ] Vendor prefixy: Nejsou použity.
- [ ] Transformace: Nastavuji origin prvků SVG s oběčencem a při překreslení animuji scale(). Není to moc, ale nic jiného mě nenapadlo.
- [ ] Animace: Animuji scale() eběšence. Potom ještě barvu a velikost tlačítek.
- [ ] Media queries: opět nejsou

### JS
- [ ] OOP: Jednotlivé stránky jsou podtřídy abstraktní stránky, která implementuje překreslování. Applikace je řízena z třídy App.
- [ ] Použití knihovny: Není implementováno
- [ ] Použití API: Pouze LocalStorage pro tabulku skóre.
- [ ] Historie: Není implementováno
- [ ] Ovládání medií: Jak jsem psal v HTML, spouštím dva zvuky. Jeden při zmáčknutí špatného tlačítka a druhý při uhodnutí slova.
- [ ] Offline: Není implementováno
- [ ] JS práce s SVG: Vykresluji do SVG oběšence, takže snad ano. Ale dělám to přes napojování prvků jako HTML text a nevytvářím jednotlivé elmenty jako jsme to dělali na cvičení.

### OSTATNÍ
- [ ] Kompletnost: Dělal jsem co jsem zmohl.
- [ ] Estetika: Dělal jsem co jsem zmohl.
